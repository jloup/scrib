package scrib

import (
	"bytes"
	"fmt"
)

type Tick struct {
	Lines []string
	Time  []string
}

type Section struct {
	Title string
	Ticks []Tick
}

type Document struct {
	Sections []Section
}

func parseTextBody(body []byte) (Document, error) {
	// Replace \r\n and \r by \n.
	body = bytes.Replace(body, []byte{13, 10}, []byte{10}, -1)
	body = bytes.Replace(body, []byte{13}, []byte{10}, -1)

	body = bytes.TrimSpace(body)

	lexer := lex("parseTextBody", string(body))

	document := Document{Sections: []Section{Section{Title: "Uncategorized"}}}

	currentSection := &document.Sections[0]
	currentLineIndex := -1
	currentTickIndex := -1

	var it item
	for it = lexer.nextItem(); it.typ != itemEOF && it.typ != itemError; it = lexer.nextItem() {
		switch it.typ {
		case itemEndOfSection:
			currentLineIndex = -1
			currentSection = &document.Sections[0]
			currentTickIndex = len(currentSection.Ticks) - 1
		case itemSectionTitle:
			document.Sections = append(document.Sections, Section{Title: it.val})
			currentSection = &document.Sections[len(document.Sections)-1]
			currentLineIndex = -1
			currentTickIndex = -1
		case itemTime:
			currentSection.Ticks[currentTickIndex].Time = append(currentSection.Ticks[currentTickIndex].Time, it.val)
		case itemTick:
			currentSection.Ticks = append(currentSection.Ticks, Tick{Lines: []string{""}})
			currentLineIndex = 0
			currentTickIndex++
		case itemLine:
			currentSection.Ticks[currentTickIndex].Lines[currentLineIndex] += it.val
		case itemNewLine:
			currentSection.Ticks[currentTickIndex].Lines = append(currentSection.Ticks[currentTickIndex].Lines, "")
			currentLineIndex = len(currentSection.Ticks[currentTickIndex].Lines) - 1
		}
	}

	if it.typ == itemError {
		return Document{}, fmt.Errorf("lexer error %+v", it)
	}

	if len(document.Sections[0].Ticks) == 0 {
		document.Sections = document.Sections[1:]
	}

	return document, nil
}

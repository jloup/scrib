package main

import (
	"bytes"
	"context"
	"fmt"
	"io"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"gitlab.com/jloup/scrib"
)

type Config struct {
	Db scrib.DbConfiguration
}

func (c *Config) SetToDefaults() {
}

var C Config

func fetchS3Mail(bucket string, key string) (io.Reader, error) {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2")},
	)

	downloader := s3manager.NewDownloader(sess)

	b := make([]byte, 100000)

	numBytes, err := downloader.Download(aws.NewWriteAtBuffer(b),
		&s3.GetObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(key),
		})

	if err != nil {
		return nil, fmt.Errorf("Unable to download item %v, %v", key, err)
	}

	return bytes.NewReader(b[0:numBytes]), nil
}

type dbInit struct {
	Db  *scrib.Db
	err error
}

type mailRx struct {
	Message scrib.Message
	err     error
}

func show(ctx context.Context, sesEvent events.SimpleEmailEvent) error {
	dbChan := make(chan dbInit)

	go func() {
		db, err := scrib.InitDb(scrib.DbConfiguration{
			User:     "XXX",
			Password: "XXX",
			Address:  "XXX",
			Database: "XXX",
		})

		dbChan <- dbInit{&db, err}
	}()

	mailChan := make(chan mailRx, len(sesEvent.Records))

	for i, record := range sesEvent.Records {
		ses := record.SES
		fmt.Printf("[%s - %s] Mail = %+v\n", record.EventVersion, record.EventSource, ses.Mail.MessageID)

		abort := false
		if i == len(sesEvent.Records)-1 {
			abort = true
		}

		go func() {
			r, err := fetchS3Mail("jloup.mail", ses.Mail.MessageID)
			if err != nil {
				mailChan <- mailRx{scrib.Message{}, err}
				return
			}

			m, err := scrib.ParseMail(r)

			mailChan <- mailRx{m, err}
			if abort {
				close(mailChan)
			}
		}()
	}

	initDb := <-dbChan
	if initDb.err != nil {
		return initDb.err
	}

	for mail := range mailChan {
		if mail.err != nil {
			return mail.err
		}

		err := scrib.ProcessIncomeEmail(*initDb.Db, mail.Message)
		if err != nil {
			return err

		}
	}

	return nil
}

func main() {
	//	utils.MustParseStdConfigFile(&C)

	lambda.Start(show)
}

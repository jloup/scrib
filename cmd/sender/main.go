package main

import (
	"context"

	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/jloup/scrib"
)

type Config struct {
	Db scrib.DbConfiguration
}

func (c *Config) SetToDefaults() {
}

var C Config

type dbInit struct {
	Db  *scrib.Db
	err error
}

type smtpInit struct {
	Client *scrib.SmtpClient
	err    error
}

func handler(ctx context.Context) error {
	dbChan := make(chan dbInit)
	smtpChan := make(chan smtpInit)

	go func() {
		db, err := scrib.InitDb(scrib.DbConfiguration{
			User:     "XXX",
			Password: "XXX",
			Address:  "XXX",
			Database: "XXX",
		})

		dbChan <- dbInit{&db, err}
	}()

	go func() {
		client, err := scrib.NewSmtpClient("XXX", "XXX", "XXX")

		smtpChan <- smtpInit{&client, err}
	}()

	initDb := <-dbChan
	if initDb.err != nil {
		return initDb.err
	}

	initSmtp := <-smtpChan
	if initSmtp.err != nil {
		return initSmtp.err
	}

	return scrib.SendMails(*initDb.Db, initSmtp.Client)
}

func main() {
	lambda.Start(handler)
}

package scrib

import (
	"fmt"
	"strconv"
	"time"
)

func parseTimeOffset(s string) (*time.Location, error) {
	if len(s) != 5 {
		return nil, fmt.Errorf("bad format: '%s'", s)
	}

	hourOffset, minuteOffset := s[0:3], s[0:1]+s[3:5]

	hour, err := strconv.ParseInt(hourOffset, 10, 64)
	if err != nil {
		return nil, err
	}
	minute, err := strconv.ParseInt(minuteOffset, 10, 32)
	if err != nil {
		return nil, err
	}

	return time.FixedZone(fmt.Sprintf("UTC%s", s), 60*int(minute)+60*60*int(hour)), nil
}

const timeLayout = "2006-01-02 15:04"

// date: 2018-12-31 14:00
// time UTC offset: +2359
func parseDate(s string, hourminuteOffset string) (time.Time, error) {
	loc, err := parseTimeOffset(hourminuteOffset)
	if err != nil {
		return time.Time{}, err
	}

	t, err := time.ParseInLocation(timeLayout, s, loc)
	if err != nil {
		return time.Time{}, err
	}

	return t, nil
}

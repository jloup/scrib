package scrib

import (
	"database/sql"
	"fmt"

	migrate "github.com/rubenv/sql-migrate"
)

var migrations []*migrate.Migration = []*migrate.Migration{
	&migrate.Migration{
		Id: "201812011200_create_initial",
		Up: []string{
			`CREATE TABLE users (
			  id         BIGSERIAL PRIMARY KEY,
        email      VARCHAR NOT NULL,
        name       VARCHAR NOT NULL,
				timezone   VARCHAR NOT NULL,
        created_at TIMESTAMP NOT NULL
			)`,
			`CREATE TABLE emails (
			  id          BIGSERIAL PRIMARY KEY,
        user_id     INTEGER NOT NULL,
			  uid         VARCHAR NOT NULL,
			  status      VARCHAR NOT NULL,
        received_at TIMESTAMP NOT NULL,
        sent_at     TIMESTAMP NOT NULL,
        created_at  TIMESTAMP NOT NULL,
			  FOREIGN KEY(user_id) REFERENCES users(id)
			)`,
			`CREATE TABLE lines (
			  id         BIGSERIAL PRIMARY KEY,
        user_id    INTEGER NOT NULL,
			  email_id   INTEGER NOT NULL,
        created_at TIMESTAMP NOT NULL,
        category   VARCHAR NOT NULL,
        content    TEXT NOT NULL,
				status     INTEGER NOT NULL,
        remind_at  TIMESTAMP,
			  FOREIGN KEY(user_id) REFERENCES users(id),
			  FOREIGN KEY(email_id) REFERENCES emails(id)
			)`,
			`CREATE INDEX IF NOT EXISTS lines_userid ON lines (user_id)`,
			`CREATE UNIQUE INDEX IF NOT EXISTS lines_user_id_category_content_status_remind_at ON lines (user_id, category, content, status, remind_at)`,
			`CREATE UNIQUE INDEX IF NOT EXISTS users_email ON users (email)`,
			`CREATE UNIQUE INDEX IF NOT EXISTS emails_uid ON emails (uid)`,
		},
		Down: []string{"DROP lines", "DROP emails", "DROP users"},
	},
}

func migrateDB(db *sql.DB) error {
	pastMigrations, err := migrate.GetMigrationRecords(db, "postgres")
	if err != nil {
		return err
	}

	logs := make([]string, 0)
	for _, migration := range migrations {
		completed := false
		for _, pastMigration := range pastMigrations {
			if pastMigration.Id == migration.Id {
				completed = true
			}
		}
		if completed {
			logs = append(logs, fmt.Sprintf("[ok] %s", migration.Id))
		} else {
			logs = append(logs, fmt.Sprintf("[x] %s", migration.Id))
		}
	}

	if len(migrations) != len(pastMigrations) {
		fmt.Printf("DATABASE MIGRATIONS:\n")
		for _, log := range logs {
			fmt.Printf("  %s\n", log)
		}
	}

	n, err := migrate.Exec(db, "postgres", migrate.MemoryMigrationSource{Migrations: migrations}, migrate.Up)
	if err != nil {
		return err
	}

	if n == 0 {
		fmt.Printf("database up-to-date: '%s'\n", migrations[len(migrations)-1].Id)
	} else {
		fmt.Printf("database schema updated to '%s'\n", migrations[len(migrations)-1].Id)
	}

	return nil
}

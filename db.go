package scrib

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	_ "github.com/lib/pq"
)

type Db struct {
	driver *sql.DB
}

type DbConfiguration struct {
	Address   string
	Database  string
	User      string
	Password  string
	VerifySsl bool
}

func connectDb(config DbConfiguration) (Db, error) {
	address := strings.Split(config.Address, ":")
	path := fmt.Sprintf(
		"user=%s password=%s host=%s port=%s dbname=%s",
		config.User,
		config.Password,
		address[0],
		address[1],
		config.Database,
	)

	if config.VerifySsl {
		path = fmt.Sprintf("%s sslmode=verify-full", path)
	}

	driver, err := sql.Open("postgres", path)

	return Db{driver: driver}, err
}

func InitDb(config DbConfiguration) (Db, error) {
	db, err := connectDb(config)
	if err != nil {
		return Db{}, err
	}

	return db, migrateDB(db.driver)
}

type EmailStatus uint8
type LineStatus uint8

const (
	EmailFetched EmailStatus = iota + 1
	EmailSeen

	LineScheduled LineStatus = 100
	LineDone      LineStatus = 200
)

type User struct {
	Id        int64
	Email     string
	Name      string
	Timezone  string
	CreatedAt time.Time
}

func scanUser(rows *sql.Rows) (User, error) {
	var user User

	err := rows.Scan(&user.Id, &user.Email, &user.Name, &user.Timezone, &user.CreatedAt)

	return user, err
}

type Email struct {
	Id         int64
	UserId     int64
	Uid        string
	Status     EmailStatus
	ReceivedAt time.Time
	SentAt     time.Time
	CreatedAt  time.Time
}

func scanEmail(rows *sql.Rows) (Email, error) {
	var email Email

	err := rows.Scan(&email.Id, &email.UserId, &email.Uid, &email.Status, &email.ReceivedAt, &email.SentAt, &email.CreatedAt)

	return email, err
}

type Line struct {
	Id        int64
	UserId    int64
	EmailId   int64
	CreatedAt time.Time
	Category  string
	Content   string
	Status    LineStatus
	RemindAt  time.Time
}

func scanLine(rows *sql.Rows) (Line, error) {
	var line Line

	err := rows.Scan(&line.Id, &line.UserId, &line.EmailId, &line.CreatedAt, &line.Category, &line.Content, &line.Status, &line.RemindAt)

	return line, err
}

type DbErrorCode uint32

const (
	NoRow DbErrorCode = 3000
)

func (d DbErrorCode) String() string {
	switch d {
	case NoRow:
		return "NoRow"
	}

	return fmt.Sprintf("Unknown[%v]", d)
}

type DbError struct {
	code DbErrorCode
	msg  string
}

func (d DbError) Error() string {
	return fmt.Sprintf("[%v] %v", d.code.String(), d.msg)
}

func DbErrorIs(err error, code DbErrorCode) bool {
	if err == nil {
		return false
	}

	dberr, ok := err.(DbError)
	if !ok {
		return false
	}

	return dberr.code == code
}

func (d Db) GetUserByEmail(email string) (User, error) {
	rows, err := d.driver.Query("SELECT * FROM users WHERE email=$1 LIMIT 1", email)
	if err != nil {
		return User{}, err
	}

	if !rows.Next() {
		return User{}, DbError{NoRow, "no user found"}
	}

	user, _ := scanUser(rows)

	rows.Close()

	return user, rows.Err()
}

func (d Db) GetUserById(id int64) (User, error) {
	rows, err := d.driver.Query("SELECT * FROM users WHERE id=$1 LIMIT 1", id)
	if err != nil {
		return User{}, err
	}

	if !rows.Next() {
		return User{}, DbError{NoRow, "no user found"}
	}

	user, _ := scanUser(rows)

	rows.Close()

	return user, rows.Err()
}

func (d Db) InsertUser(user User) (User, error) {
	stmt, err := d.driver.Prepare("INSERT INTO users(email, name, timezone, created_at) VALUES($1,$2,$3,$4)")
	if err != nil {
		return user, err
	}

	_, err = stmt.Exec(user.Email, user.Name, user.Timezone, time.Now().UTC())

	if err != nil {
		return user, err
	}

	return d.GetUserByEmail(user.Email)
}

func (d Db) GetEmailByUid(uid string) (Email, error) {
	rows, err := d.driver.Query("SELECT * FROM emails WHERE uid=$1 LIMIT 1", uid)
	if err != nil {
		return Email{}, err
	}

	if !rows.Next() {
		return Email{}, DbError{NoRow, "no email found"}
	}

	email, _ := scanEmail(rows)

	rows.Close()

	return email, rows.Err()
}

func (d Db) InsertEmail(email Email) (Email, error) {
	stmt, err := d.driver.Prepare("INSERT INTO emails(user_id, uid, status, received_at, sent_at, created_at) VALUES($1,$2,$3,$4,$5,$6)")
	if err != nil {
		return email, err
	}

	_, err = stmt.Exec(email.UserId, email.Uid, email.Status, email.ReceivedAt, email.SentAt, time.Now().UTC())

	if err != nil {
		return email, err
	}

	return d.GetEmailByUid(email.Uid)
}

func (d Db) GetLine(userId int64, category string, content string, status LineStatus) (Line, error) {
	rows, err := d.driver.Query("SELECT * FROM lines WHERE user_id=$1 AND category=$2 AND content=$3 AND status=$4 LIMIT 1", userId, category, content, status)
	if err != nil {
		return Line{}, err
	}

	if !rows.Next() {
		return Line{}, DbError{NoRow, "no line found"}
	}

	line, _ := scanLine(rows)

	rows.Close()

	return line, rows.Err()
}

func (d Db) InsertLine(line Line) (Line, error) {
	stmt, err := d.driver.Prepare("INSERT INTO lines(user_id, email_id, created_at, category, content, status, remind_at) VALUES($1,$2,$3,$4,$5,$6,$7)")
	if err != nil {
		return line, err
	}

	_, err = stmt.Exec(line.UserId, line.EmailId, time.Now().UTC(), line.Category, line.Content, line.Status, line.RemindAt)
	if err != nil {
		return line, err
	}

	return d.GetLine(line.UserId, line.Category, line.Content, line.Status)
}

func (d Db) GetScheduledLines(date time.Time) ([]Line, error) {
	rows, err := d.driver.Query("SELECT * FROM lines WHERE remind_at <= $1 AND status=$2", date, LineScheduled)

	if err != nil {
		return nil, err
	}

	lines := make([]Line, 0)

	for rows.Next() {
		line, err := scanLine(rows)
		if err != nil {
			return nil, err
		}

		lines = append(lines, line)
	}

	rows.Close()

	return lines, rows.Err()
}

func (d Db) UpdateLineStatus(line Line, status LineStatus) (Line, error) {
	stmt, err := d.driver.Prepare("UPDATE lines SET status=$1 WHERE id=$2")
	if err != nil {
		return line, err
	}

	_, err = stmt.Exec(status, line.Id)
	if err != nil {
		return line, err
	}

	return d.GetLine(line.UserId, line.Category, line.Content, status)
}

package scrib

import (
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"strings"
	"time"

	imap "github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message/mail"
	sasl "github.com/emersion/go-sasl"
	smtp "github.com/emersion/go-smtp"
)

type ImapClient struct {
	c *client.Client
}

type SmtpClient struct {
	address        string
	smtpAuthClient sasl.Client
}

type Message struct {
	Header         mail.Header
	From           []*mail.Address
	Location       *time.Location
	LocationOffset string
	Date           time.Time
	Subject        string
	TextBody       []byte
	Uid            string
}

func NewSmtpClient(address string, user, password string) (SmtpClient, error) {
	auth := sasl.NewPlainClient("", user, password)

	smtpClient, err := smtp.Dial(address)
	if err != nil {
		return SmtpClient{}, err
	}

	err = smtpClient.StartTLS(&tls.Config{InsecureSkipVerify: true})
	if err != nil {
		return SmtpClient{}, err
	}

	err = smtpClient.Auth(auth)
	if err != nil {
		return SmtpClient{}, err
	}

	client := SmtpClient{
		address:        address,
		smtpAuthClient: auth,
	}

	return client, nil
}

func NewImapClient(address, user, password string) (ImapClient, error) {
	c, err := client.DialTLS(address, &tls.Config{InsecureSkipVerify: true})

	if err != nil {
		return ImapClient{}, err
	}

	auth := sasl.NewPlainClient("", user, password)

	err = c.Authenticate(auth)
	if err != nil {
		return ImapClient{}, err
	}

	client := ImapClient{
		c: c,
	}

	return client, nil
}

func (s *SmtpClient) SendMail(from string, to string, subject string, body string) error {
	y := fmt.Sprintf("To: %s\r\nReturn-Path: %s\r\nSubject: %s\r\n\r\n%s\r\n", to, "jeanloup.jamet@gmail.com", subject, body)
	fmt.Println(y)
	r := strings.NewReader(y)

	return smtp.SendMail(s.address, s.smtpAuthClient, from, []string{to}, r)
}

func (i *ImapClient) ListMbxes() ([]imap.MailboxInfo, error) {
	mailboxes := make(chan *imap.MailboxInfo, 10)
	done := make(chan error, 1)
	go func() {
		done <- i.c.List("", "*", mailboxes)
	}()

	infos := make([]imap.MailboxInfo, 0)

	for m := range mailboxes {
		infos = append(infos, *m)
	}

	if err := <-done; err != nil {
		return nil, err
	}

	return infos, nil
}

func (i *ImapClient) SelectMbx(name string) (*imap.MailboxStatus, error) {
	return i.c.Select(name, false)
}

func (i *ImapClient) MbxStatus() *imap.MailboxStatus {
	return i.c.Mailbox()
}

func ParseMail(r io.Reader) (Message, error) {
	reader, err := mail.CreateReader(r)
	if err != nil {
		return Message{}, err
	}

	textBody := make([]byte, 0)
	for {
		p, err := reader.NextPart()
		if err == io.EOF {
			break
		}
		if err != nil {
			return Message{}, err
		}

		if h, ok := p.Header.(*mail.InlineHeader); ok && strings.HasPrefix(h.Get("Content-Type"), "text/plain") {
			textBody, err = ioutil.ReadAll(p.Body)
			if err != nil {
				return Message{}, err
			}
		}

	}

	addresses, err := reader.Header.AddressList("From")
	if err != nil {
		return Message{}, err
	}

	subject, err := reader.Header.Subject()
	if err != nil {
		return Message{}, err
	}

	dateHeader := reader.Header.Get("Date")
	offset := dateHeader[len(dateHeader)-5:]
	location, err := parseTimeOffset(offset)
	if err != nil {
		return Message{}, err
	}

	date, err := reader.Header.Date()
	if err != nil {
		return Message{}, err
	}

	return Message{Header: reader.Header,
		Date:           date.UTC(),
		Location:       location,
		LocationOffset: offset,
		Subject:        subject,
		TextBody:       textBody,
		Uid:            reader.Header.Get("Message-Id"),
		From:           addresses,
	}, nil
}

func (i *ImapClient) FetchAllEmails() ([]Message, error) {
	currentSeqNum := i.MbxStatus().Messages

	return i.FetchMessages(0, currentSeqNum)
}

func (i *ImapClient) FetchMessages(from, to uint32) ([]Message, error) {
	seqset := new(imap.SeqSet)
	seqset.AddRange(from, to)
	messages := make([]Message, 0, to-from)
	messagesChannel := make(chan *imap.Message, to-from)

	section := &imap.BodySectionName{}
	items := []imap.FetchItem{section.FetchItem(), imap.FetchUid, imap.FetchFlags}

	done := make(chan error, 1)

	go func() {
		done <- i.c.Fetch(seqset, items, messagesChannel)
	}()

	for message := range messagesChannel {
		body := message.GetBody(section)
		if body == nil {
			return nil, fmt.Errorf("mail body is nil")
		}

		msg, err := ParseMail(body)
		if err != nil {
			return nil, err
		}

		messages = append(messages, msg)
	}

	if err := <-done; err != nil {
		return messages, err
	}

	return messages, nil
}

func (i *ImapClient) NewPoller(doneChan chan struct{}, interval time.Duration) (func() error, chan Message) {
	messagesChan := make(chan Message)

	return func() error {
		ticker := time.NewTicker(interval)

		defer close(messagesChan)
		defer ticker.Stop()

		currentSeqNum = 0

		for {
			select {
			case <-ticker.C:
				err := i.Noop()
				if err != nil {
					return err
				}

				seqNum := i.MbxStatus().Messages
				if seqNum > currentSeqNum {
					messages, err := i.FetchMessages(currentSeqNum+1, seqNum)
					if err != nil {
						return err
					}

					for _, message := range messages {
						messagesChan <- message
					}
				}

				currentSeqNum = seqNum

			case <-doneChan:
				return nil
			}
		}
	}, messagesChan
}

func (i *ImapClient) Noop() error {
	return i.c.Noop()
}

func (i *ImapClient) Logout() error {
	return i.c.Logout()
}

package scrib

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

type itemType uint8

const (
	leftSectionTitleMeta = "#"
	rightSectionMeta     = "\n\n"
	leftTimeRune         = '\\'
	leftTickMeta         = "-"
	rightTimeRune        = ' '
	eof                  = 0
)

const (
	itemError itemType = iota

	itemLine
	itemTick
	itemEndOfSection
	itemSectionTitle
	itemNewLine
	itemText
	itemTime
	itemEOF
)

type item struct {
	typ  itemType
	val  string
	line int
}

type stateFn func(*lexer) stateFn

type lexer struct {
	name  string // used only for error reports.
	input string // the string being scanned.
	start int    // start position of this item.
	pos   int    // current position in the input.
	width int    // width of last rune read from input.
	line  int
	items chan item // channel of scanned items.
	state stateFn
}

func (itype itemType) String() string {
	switch itype {
	case itemError:
		return "Error"
	case itemEOF:
		return "EOF"
	case itemLine:
		return "Line"
	case itemTick:
		return "Tick"
	case itemText:
		return "Text"
	case itemEndOfSection:
		return "EndOfSection"
	case itemSectionTitle:
		return "SectionTitle"
	case itemNewLine:
		return "NewLine"
	case itemTime:
		return "Time"
	}
	panic(fmt.Sprintf("BUG: Unknown type '%d'.", int(itype)))
}

func (item item) String() string {
	return fmt.Sprintf("%s: %s (line %v)", item.typ.String(), item.val, item.line)
}

func lex(name, input string) *lexer {
	l := &lexer{
		name:  name,
		input: input,
		state: lexText,
		line:  1,
		items: make(chan item, 2),
	}

	return l
}

func (l *lexer) nextItem() item {
	for {
		select {
		case item := <-l.items:
			return item
		default:
			l.state = l.state(l)
		}
	}
}

func (l *lexer) emit(t itemType) {
	l.items <- item{t, l.input[l.start:l.pos], l.line}
	l.start = l.pos
}

func (l *lexer) emitTrim(typ itemType) {
	l.items <- item{typ, strings.TrimSpace(l.input[l.start:l.pos]), l.line}
	l.start = l.pos
}

func (l *lexer) errorf(format string, values ...interface{}) stateFn {
	l.items <- item{
		itemError,
		fmt.Sprintf(format, values...),
		l.line,
	}

	return nil
}

func (l *lexer) ignore() {
	l.start = l.pos
}

func (l *lexer) backup() {
	l.pos -= l.width
	if l.pos < len(l.input) && l.input[l.pos] == '\n' {
		l.line--
	}
}

func (l *lexer) next() (rune rune) {
	if l.pos >= len(l.input) {
		l.width = 0
		return eof
	}
	rune, l.width = utf8.DecodeRuneInString(l.input[l.pos:])
	l.pos += l.width

	if rune == '\n' {
		l.line++
	}

	return rune
}

func (l *lexer) peek() rune {
	rune := l.next()
	l.backup()
	return rune
}

func (l *lexer) peekback() rune {
	l.backup()
	return l.next()
}

func lexText(l *lexer) stateFn {
	for {
		if l.pos == 0 && strings.HasPrefix(l.input[l.pos:], leftSectionTitleMeta) {
			if l.pos > l.start {
				l.emitTrim(itemText)
			}
			return lexLeftSectionTitleMeta
		}

		if strings.HasPrefix(l.input[l.pos:], leftSectionTitleMeta) && l.peekback() == '\n' {
			if l.pos > l.start {
				l.emitTrim(itemText)
			}
			l.pos++
			l.ignore()
			return lexLeftSectionTitleMeta
		}

		if l.pos == 0 && strings.HasPrefix(l.input[l.pos:], leftTickMeta) {
			return lexTick
		}

		if l.peek() == '\n' {
			l.next()
			l.ignore()
			return lexOrphanNewLine
		}

		if l.next() == eof {
			break
		}
	}

	if l.pos > l.start {
		l.emitTrim(itemText)
	}

	l.emit(itemEOF)
	return nil
}

func lexOrphanNewLine(l *lexer) stateFn {
	for {
		switch r := l.next(); {
		case r == eof:
			l.emit(itemEOF)
			return nil
		case r == '-':
			l.backup()
			return lexTick
		case r == ' ':
			l.ignore()
		default:
			l.backup()
			return lexText
		}
	}
}

func lexRightSectionMeta(l *lexer) stateFn {
	l.emitTrim(itemLine)
	l.emit(itemEndOfSection)
	l.pos += len(rightSectionMeta)
	l.line += 2
	return lexText
}

func lexLeftSectionTitleMeta(l *lexer) stateFn {
	l.pos += len(leftSectionTitleMeta)
	l.ignore()
	return lexInsideSectionTitle
}

func lexInsideSectionTitle(l *lexer) stateFn {
	for {
		if strings.HasPrefix(l.input[l.pos:], rightSectionMeta) {
			return l.errorf("empty section")
		}
		switch r := l.next(); {
		case r == eof:
			return l.errorf("empty section")
		case r == '\n':
			l.backup()
			l.emitTrim(itemSectionTitle)
			l.next()
			l.ignore()
			return lexInsideSection
		}
	}
}

func lexInsideSection(l *lexer) stateFn {
	for {

		if l.peek() == ' ' {
			l.next()
			l.ignore()
			return lexInsideSection
		}

		if strings.HasPrefix(l.input[l.pos:], leftTickMeta) {
			return lexTick
		}

		return l.errorf("section should start with '%v'", leftTickMeta)
	}
}

func lexTick(l *lexer) stateFn {
	l.pos += len(leftTickMeta)
	l.ignore()
	l.emit(itemTick)

	return lexLine
}

func lexNewLine(l *lexer) stateFn {
	for {
		switch r := l.next(); {
		case r == eof:
			l.emit(itemEOF)
			return nil
		case r == '-':
			l.backup()
			return lexTick
		case r == ' ':
			l.ignore()
		default:
			l.backup()
			l.emit(itemNewLine)
			return lexLine
		}
	}
}

func lexLine(l *lexer) stateFn {
	for {
		if strings.HasPrefix(l.input[l.pos:], rightSectionMeta) {
			return lexRightSectionMeta
		}

		switch r := l.next(); {
		case r == eof:
			l.emitTrim(itemLine)
			l.emit(itemEOF)
			return nil
		case r == '\n':
			l.backup()
			l.emit(itemLine)
			l.next()
			l.ignore()
			return lexNewLine
		case r == leftTimeRune:
			l.backup()
			l.emit(itemLine)
			return lexLeftTimeRune
		}
	}
}

func lexLeftTimeRune(l *lexer) stateFn {
	l.pos++
	l.ignore()
	return lexInsideTime
}

func lexInsideTime(l *lexer) stateFn {
	for {
		if strings.HasPrefix(l.input[l.pos:], rightSectionMeta) {
			l.emit(itemTime)
			return lexRightSectionMeta
		}

		switch r := l.next(); {
		case r == eof:
			l.backup()
			l.emit(itemTime)
			l.emit(itemEOF)
			return nil
		case r == '\n':
			l.backup()
			l.emit(itemTime)
			l.next()
			l.ignore()
			return lexNewLine

		case r == rightTimeRune:
			l.backup()
			l.emit(itemTime)
			l.next()
			l.ignore()
			return lexLine
		}
	}

}

package scrib

import (
	"fmt"
	"strings"
	"time"
)

const mailLogFormatter = `
===================================
[%v]
From: '%s'
Date: %s
Subject: %s

%s
===================================
`

func ProcessIncomeEmail(db Db, msg Message) error {
	from := msg.From[0]
	now := time.Now().UTC()
	userNow := now.In(msg.Location)

	user, err := db.GetUserByEmail(from.Address)
	if err != nil && !DbErrorIs(err, NoRow) {
		return err
	}

	if DbErrorIs(err, NoRow) {
		user = User{Email: from.Address, Name: from.Name, Timezone: msg.LocationOffset}
		user, err = db.InsertUser(user)
		if err != nil {
			return err
		}
	}

	email, err := db.GetEmailByUid(msg.Uid)
	if err != nil && !DbErrorIs(err, NoRow) {
		return err
	}

	if DbErrorIs(err, NoRow) {
		email = Email{UserId: user.Id, Uid: msg.Uid, Status: EmailFetched, SentAt: msg.Date}
		email, err = db.InsertEmail(email)
		if err != nil {
			return err
		}
	} else {
		return nil
	}

	doc, err := parseTextBody(msg.TextBody)

	if err != nil {
		return err
	}

	for _, section := range doc.Sections {
		for _, tick := range section.Ticks {
			lines := make([]string, 0)
			for _, line := range tick.Lines {
				lines = append(lines, strings.TrimSpace(line))
			}

			remindAt := milestoneToDate(msg.Date, Morning, *msg.Location)
			for _, t := range tick.Time {
				remindAt = milestoneToDate(remindAt, Milestone(t), *msg.Location)
			}

			if remindAt.Sub(userNow.UTC()) < 0 {
				if (userNow.Hour() + 1) >= EVENING {
					remindAt = milestoneToDate(userNow, Tomorrow, *msg.Location)
					remindAt = milestoneToDate(remindAt, Morning, *msg.Location)
				} else if (userNow.Hour() + 1) <= MORNING {
					remindAt = milestoneToDate(userNow, Morning, *msg.Location)
				} else {
					remindAt = milestoneToDate(userNow, Evening, *msg.Location)
				}

			}

			line, err := db.GetLine(user.Id, section.Title, strings.Join(lines, "\\n"), LineScheduled)
			if err != nil && !DbErrorIs(err, NoRow) {
				return err
			}

			if DbErrorIs(err, NoRow) {
				line = Line{
					UserId:   user.Id,
					EmailId:  email.Id,
					Category: section.Title,
					Content:  strings.Join(lines, "\\n"),
					Status:   LineScheduled,
					RemindAt: remindAt,
				}

				line, err = db.InsertLine(line)
				if err != nil {
					return err
				}
			}
		}
	}

	fmt.Printf(mailLogFormatter, msg.Uid, from.Address, msg.Subject, msg.Date, string(msg.TextBody))

	return nil
}

type Milestone string

const (
	EndOfWeek Milestone = "end-of-week"
	Evening             = "evening"
	Morning             = "morning"
	Am                  = "am"
	Pm                  = "pm"
	NextWeek            = "next-week"
	Tomorrow            = "tomorrow"
	Tonight             = "tonight"
	Today               = "today"
)

// Reminders are the following:
// Everyday
//   08:00 am (morning, next-week)
//   20:00 pm (tonight, end-of-week)

const MORNING = 8
const EVENING = 20

func milestoneToDate(from time.Time, milestone Milestone, location time.Location) time.Time {
	from = from.UTC()

	year := from.Year()
	month := from.Month()
	day := from.Day()
	hour := from.Hour() + 1
	minute := from.Minute()

	switch milestone {
	case EndOfWeek:
		diff := time.Friday - from.Weekday()
		if diff < 0 {
			diff = 7 - from.Weekday() + time.Friday
		}

		day += int(diff)
		hour = EVENING
	case NextWeek:
		diff := time.Monday - from.Weekday()
		if diff < 0 {
			diff = 7 - from.Weekday() + time.Monday
		}

		day += int(diff)
		hour = MORNING
	case Today:
	case Tomorrow:
		day += 1
	case Morning, Am:
		hour = MORNING
		minute = 0
	case Evening, Pm:
		hour = EVENING
		minute = 0
	}

	return time.Date(year, month, day, hour, minute, 0, 0, &location).UTC()
}

func SendMails(db Db, client *SmtpClient) error {
	now := time.Now().UTC()
	lines, err := db.GetScheduledLines(now)
	if err != nil {
		return err
	}

	byUser := make(map[int64][]Line)
	for _, line := range lines {
		if _, ok := byUser[line.UserId]; !ok {
			byUser[line.UserId] = make([]Line, 0)
		}

		fmt.Printf("%v %v %+q\n", line.UserId, line.Category, line.Content)

		byUser[line.UserId] = append(byUser[line.UserId], line)
		_, err = db.UpdateLineStatus(line, LineDone)
		if err != nil {
			return err
		}
	}

	for userId, linesu := range byUser {
		user, err := db.GetUserById(userId)
		if err != nil {
			return err
		}

		o := buildEmailBody(now, user, linesu)
		if o != "" {

			fmt.Println("->", user.Email)
			err = client.SendMail("write@scrib.xyz", user.Email, "Hey", o)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func buildEmailBody(d time.Time, user User, lines []Line) string {
	sections := make(map[string]string)

	for _, line := range lines {
		parts := strings.Split(line.Content, "\\n")
		sections[line.Category] += "\n  - " + parts[0]
		for _, part := range parts[1:] {
			sections[line.Category] += "\n    " + part
		}
	}

	location, _ := parseTimeOffset(user.Timezone)

	var greetings [2]string
	if d.In(location).Hour()-1 <= MORNING {
		greetings[0] = "Good morning,"
		greetings[1] = "Go."
	} else {
		greetings[0] = "Good evening,"
		greetings[1] = "Have a nice night."
	}

	var output string

	output = fmt.Sprintf("%s\r\n\r\n", greetings[0])

	for cat, section := range sections {
		output += fmt.Sprintf("# %s%s\r\n\r\n", cat, section)
	}

	output += fmt.Sprintf("%s", greetings[1])

	return output
}
